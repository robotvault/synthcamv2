#include<iosfwd>
#include <icarus/render/renderutils.h>

#include<cmath>
#include <vector>
#include<vector_functions.h>


#include <cuda.h>
#include <cuda_runtime.h>
#include <cutil_gl_error.h>
#include <cuda_gl_interop.h>

#include <vector_types.h>

#include <gvars3/default.h>
#include <gvars3/gvars3.h>

#include <cvd/image_io.h>
#include <boost/timer/timer.hpp>

#include <pangolin/pangolin.h>

#include <VaFRIC/VaFRIC.h>

#include <TooN/Cholesky.h>

#include<cvd/gl_helpers.h>

#include <assimp/Importer.hpp>
#include <assimp/mesh.h>
#include <assimp/postprocess.h>
#include <assimp/scene.h>



using namespace pangolin;
using namespace TooN;
using namespace CVD;

void DrawCamera(TooN::SE3<> world_from_cam, float end_pt, float line_width , bool do_inverse)
{
    glPushMatrix();

    Vector<6> rot_trans = world_from_cam.ln();

    static int i = 0;

    world_from_cam = TooN::SE3<>(rot_trans);

    if ( do_inverse )
    {
        glMultMatrix(world_from_cam.inverse());
        if ( i == 0 )
        {
            std::cout << " world_from_cam inverse = " << world_from_cam << std::endl;
            i++;
        }

    }
    else
    {
        glMultMatrix(world_from_cam);
    }


    glColor3f(1.0,0.0,1.0);
    glShadeModel(GL_SMOOTH);
    glTranslatef(0,0,0);

    /// Ref: http://www.alpcentauri.info/glutsolidsphere.htm

    glutSolidSphere(0.02f, 10.0f, 2.0f);

    glLineWidth(line_width);

    glColor3f(1.0,0.0,0.0);
    glBegin(GL_LINES);
      glVertex3f(0.0f, 0.0f, 0.0f);
      glVertex3f(end_pt, 0.0f, 0.0f);
    glEnd();

    glColor3f(0.0,1.0,0.0);
    glBegin(GL_LINES);
      glVertex3f(0.0f, 0.0f, 0.0f);
      glVertex3f(0.0f, end_pt, 0.0f);
    glEnd();

    glColor3f(0.0,0.0,1.0);
    glBegin(GL_LINES);
      glVertex3f(0.0f, 0.0f, 0.0f);
      glVertex3f(0.0f, 0.0f, end_pt);
    glEnd();

    glPopMatrix();

}


void getVertices3DModel(float*& vertexArray, int& numVerts,
                        std::string meshName)
{
    Assimp::Importer importer;

    const aiScene *scene = importer.ReadFile(meshName,
                                             aiProcessPreset_TargetRealtime_Fast);
    int totalfaces = 0;

    std::cout << "number of meshes = " << scene->mNumMeshes << std::endl;

    for(int i = 0 ; i < scene->mNumMeshes; i++)
    {
        aiMesh* mesh = scene->mMeshes[i];
        totalfaces  += mesh->mNumFaces;
    }

    numVerts    = totalfaces*3;

    std::cout << "total vertices = " << numVerts << std::endl;

    vertexArray = new float[totalfaces*3*3];

    for(int m = 0 ; m < scene->mNumMeshes; m++)
    {
        aiMesh *mesh = scene->mMeshes[m];

//        if ( m <= 1) continue;

        std::cout<<"numFaces = " << mesh->mNumFaces << std::endl;

        for(unsigned int i=0;i<mesh->mNumFaces;i++)
        {
            const aiFace& face = mesh->mFaces[i];

            for(int j=0;j<3;j++)
            {
                aiVector3D pos = mesh->mVertices[face.mIndices[j]];
                memcpy(vertexArray,&pos,sizeof(float)*3);
                vertexArray+=3;
            }
        }
    }

    vertexArray-=totalfaces*3*3;

    std::cout<<"Total number of vertices = " << numVerts << std::endl;
}


int main(void)
{

    float* vertexArray;
    int numVerts;

//    getVertices3DModel(vertexArray,numVerts,"../../online_conversion/room_89_chair_meshlab.obj");

    getVertices3DModel(vertexArray,numVerts,"../../online_conversion/room_89_BE-457_chair_meshlab.obj");

    std::cout<<"Total number of vertices = " << numVerts << std::endl;

    int width  = 640;
    int height = 480;

    int w_width  = 640;
    int w_height = 480;


    pangolin::CreateGlutWindowAndBind("Main",w_width+150,w_height);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glewInit();

    /// Create a Panel
    pangolin::View& d_panel = pangolin::CreatePanel("ui")
            .SetBounds(1.0, 0.0, 0, pangolin::Attach::Pix(150));

    // Define Camera Render Object (for view / scene browsing)
    pangolin::OpenGlRenderState s_cam(
      ProjectionMatrix(640,480,420,420,320,240,0.1,1000),
      ModelViewLookAt(-0,2,-2, 0,0,0, AxisY)
    );

    const int UI_WIDTH = 180;

    // Add named OpenGL viewport to window and provide 3D Handler
    View& d_cam = pangolin::Display("cam")
      .SetBounds(0.0, 1.0, Attach::Pix(UI_WIDTH), 1.0, -640.0f/480.0f)
      .SetHandler(new Handler3D(s_cam));


    // Create vertex and colour buffer objects and register them with CUDA
    GlBufferCudaPtr vertex_array(
        GlArrayBuffer, width * height * sizeof(float4),
        cudaGraphicsMapFlagsWriteDiscard, GL_STREAM_DRAW
    );
    GlBufferCudaPtr colour_array(
        GlArrayBuffer, width * height * sizeof(uchar4),
        cudaGraphicsMapFlagsWriteDiscard, GL_STREAM_DRAW
    );


    float theta = 0;
    float phi = 0;

///    http://www.cmu.edu/biolphys/deserno/pdf/sphere_equi.pdf:
///    An alternative way of doing it.

    while( !pangolin::ShouldQuit() )
    {
        static Var<float>radius("ui.radius",1,1,10);
        static Var<int>n_levels("ui.n_levels",3,1,100);

        static Var<int>l_levels("ui.l_levels",0,0,10);

        static Var<float> end_pt("ui.end_pt",0.15,0,10);
        static Var<float> line_width("ui.line_width",2,0,100);

        static Var<bool>write_poses("ui.write_poses",false);

        static Var<float>rx("ui.rx",0,0,10);
        static Var<float>ry("ui.ry",0,0,10);
        static Var<float>rz("ui.rz",0,0,10);

        static Var<float>tx("ui.tx",0,-10,10);
        static Var<float>ty("ui.ty",0,-10,10);
        static Var<float>tz("ui.tz",0,-10,10);

        std::vector<float3>sphere_points;
        std::vector<TooN::SE3<> > poses;

        for(int i = (int)n_levels; i >=(int)((int)n_levels-(int)l_levels) ; i--)
        {
            theta = (i * M_PI/2.0f) / ( (int)n_levels * 1.0f) ;

            for(int j = 0; j < 4*i; j++)
            {
                phi = ( (1/2.0f) * j * (float)M_PI) / i ; /// [0, 2 * M_PI]

                float3 point = make_float3(radius*sin(theta)*cos(phi),
                                           radius*sin(theta)*sin(phi),
                                           radius*cos(theta));

                sphere_points.push_back(point);

                TooN::Matrix<3>RotMat = TooN::Zeros(3);

                /// http://www.iac.ethz.ch/edu/courses/bachelor/veranstaltungen/environmental_fluid_dynamics/AD3
                /// Page 4!

                TooN::Vector<3>zAxis = -1.0f* TooN::makeVector(point.x,point.y,point.z)/(float)radius;
                TooN::Vector<3>xAxis =  -1.0f*TooN::makeVector(-sin(phi),cos(phi),0);
                TooN::Vector<3>yAxis =  zAxis ^ xAxis;

                RotMat.T()[0] = xAxis;
                RotMat.T()[1] = yAxis;
                RotMat.T()[2] = zAxis;

                TooN::SO3<>RMat(RotMat);

                TooN::SE3<>T = TooN::SE3<>(RMat,TooN::makeVector(point.x,
                                                                 point.y,
                                                                 point.z));

                TooN::SE3<>T_rot = TooN::SE3<>(TooN::SO3<>(TooN::makeVector((float)rx,(float)ry,(float)rz)),
                                               TooN::makeVector((float)tx,(float)ty,(float)tz));

                poses.push_back(T_rot*T);

                std::cout<<"T = " << T << std::endl;

            }

       }

        if ( write_poses && poses.size() )
        {
            ofstream ofile("hemisphere_poses.txt");

            for(int i = 0; i < poses.size(); i++)
            {
                ofile << poses.at(i).ln() << std::endl;
            }

            ofile.close();

            write_poses  = false;
        }

        d_cam.ActivateScissorAndClear(s_cam);
        glEnable(GL_DEPTH_TEST);
        glColor3f(1.0,1.0,1.0);


        glColor3f(1.0,0.0,0.0);
        glBegin(GL_LINES);
          glVertex3f(0.0f, 0.0f, 0.0f);
          glVertex3f(7, 0.0f, 0.0f);
        glEnd();

        glColor3f(0.0,1.0,0.0);
        glBegin(GL_LINES);
          glVertex3f(0.0f, 0.0f, 0.0f);
          glVertex3f(0.0f, 7, 0.0f);
        glEnd();

        glColor3f(0.0,0.0,1.0);
        glBegin(GL_LINES);
          glVertex3f(0.0f, 0.0f, 0.0f);
          glVertex3f(0.0f, 0.0f, 7);
        glEnd();



        glBegin(GL_POINTS);

            for(int i = 0; i < sphere_points.size();i++)
            {
                glVertex3f(sphere_points.at(i).x,
                           sphere_points.at(i).y,
                           sphere_points.at(i).z);
            }

        glEnd();


        glEnable(GL_DEPTH_TEST);
        glColor3f(1.0f,1.0f,1.0f);

        glEnableClientState(GL_VERTEX_ARRAY);

        glVertexPointer(3,GL_FLOAT,0,vertexArray);

        /// Turn on wireframe mode
        glPolygonMode(GL_FRONT, GL_LINE);
        glPolygonMode(GL_BACK, GL_LINE);

        glDrawArrays(GL_TRIANGLES,0,numVerts);

        /// Turn off wireframe mode
        glPolygonMode(GL_FRONT, GL_FILL);
        glPolygonMode(GL_BACK, GL_FILL);

        glDisableClientState(GL_VERTEX_ARRAY);



//        glBegin(GL_LINES);

//        for(int i = 0; i < sphere_points.size();i++)
//        {
//            glColor3f(1.0,0.0,1.0f);

//            glVertex3f(0,0,0);

//            glVertex3f(sphere_points.at(i).x,
//                       sphere_points.at(i).y,
//                       sphere_points.at(i).z);
//        }

//        glEnd();


        for(int i = 0; i < poses.size(); i++)
        {

            DrawCamera(poses.at(i),
                       (float)end_pt,
                       (float)line_width,
                       false);
        }


        d_panel.Render();
        pangolin::FinishGlutFrame();
    }
}
