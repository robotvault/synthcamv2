//#include<iostream>
//#include<VaFRIC/VaFRIC.h>


//int checkNormal(std::vector<float>& normal_array_x,
//                std::vector<float>& normal_array_y,
//                std::vector<float>& normal_array_z,
//                int img_height,
//                int img_width,
//                std::string& normal_file_name_in_png)
//{

//    CVD::Image< CVD::Rgb<CVD::byte> >normalImage(CVD::ImageRef(img_width,img_height));

//    int i = 0;

//    for(int yy = 0; yy < img_height ; yy++ )
//    {
//        for(int xx = 0; xx < img_width; xx++ )
//        {
//            i = yy*img_width + xx;

//            normalImage[CVD::ImageRef(xx,yy)] = CVD::Rgb<CVD::byte>(
//                        (unsigned char)(normal_array_x[i]*128.f+128.f),
//                        (unsigned char)(normal_array_y[i]*128.f+128.f),
//                        (unsigned char)(normal_array_z[i]*128.f+128.f)
//                        );
//        }
//    }

//    CVD::img_save(normalImage,normal_file_name_in_png.c_str());

//}


//void writeNormalImages(int n_images)
//{
//    for(int f = 0 ; f < n_images; f++ )
//    {
//        char normal_x_fileName[150];
//        char normal_y_fileName[150];
//        char normal_z_fileName[150];

//        sprintf(normal_x_fileName,"../data/normal_x_%04d.txt",f);
//        sprintf(normal_y_fileName,"../data/normal_y_%04d.txt",f);
//        sprintf(normal_z_fileName,"../data/normal_z_%04d.txt",f);

//        ifstream normal_file_x(normal_x_fileName);
//        ifstream normal_file_y(normal_y_fileName);
//        ifstream normal_file_z(normal_z_fileName);

//        std::vector<float>normal_array_x;
//        std::vector<float>normal_array_y;
//        std::vector<float>normal_array_z;


//        int i = 0;
//        float val =0;
//        while(1)
//        {
//            normal_file_x >> val;

//            if ( normal_file_x.eof() )
//            {
//                break;
//            }

//            normal_array_x.push_back( val );

//            i++;
//        }

//        i = 0;
//        while(1)
//        {
//            normal_file_y >> val;

//            if ( normal_file_y.eof() )
//            {
//                break;
//            }

//            normal_array_y.push_back( val );

//            i++;
//        }

//        i = 0;
//        while(1)
//        {
//            normal_file_z >> val;

//            if ( normal_file_z.eof() )
//            {
//                break;
//            }

//            normal_array_z.push_back( val );

//            i++;

//        }

//        char normalfileName[50];
//        sprintf(normalfileName,"../data/normal_converted_image_%04d.png",f);

//        std::string fileName(normalfileName);

//        checkNormal(normal_array_x,
//                    normal_array_y,
//                    normal_array_z,
//                    120,
//                    160,
//                    fileName);

//        normal_array_x.clear();
//        normal_array_y.clear();
//        normal_array_z.clear();

//        std::cout<<"converted : " << f << std::endl;
//    }
//}


//int main(void)
//{

//    int __width  = 80*2;
//    int __height = 60*2;

//    float u0 = 319.5 / (640/__width);
//    float v0 = 239.5 / (480/__height);

//    float fx =   481.20 / (640/__width);
//    float fy = - 480.00 / (480/__height);

//    std::cout<<"u0 = " << u0 <<", v0 = " << v0 <<", fx = " << fx <<", fy = " << fy << std::endl;

//    dataset::vaFRIC VaFRIC_dataset("/scratch/workspace/code/new_scene_dataset/room_89/traj_01/160x120",
//                                   __width,
//                                   __height,
//                                   u0,
//                                   v0,
//                                   fx,
//                                   fy);


//    int total_images = VaFRIC_dataset.getNumberofImageFiles();

//    std::vector<float>normal_array;

//    for(int i = 0; i < total_images; i++)
//    {
//        VaFRIC_dataset.getNormalsFromDepth(i,0,normal_array);

//        char normal_file_name[50];
//        sprintf(normal_file_name,"../data/traj_02/normal_file_name_%04d.png",i);
//        std::string normal_png_file(normal_file_name);
//        VaFRIC_dataset.convertNormals2Image(i,0,normal_array,normal_png_file);

//        std::cout<<"i = " << i << std::endl;

//        char depthfileName[50];

//        ofstream normal_file_x;
//        ofstream normal_file_y;
//        ofstream normal_file_z;

//        std::vector<float>normal_array_x;
//        std::vector<float>normal_array_y;
//        std::vector<float>normal_array_z;

//        int ind = 0;

//        for(int yy = 0; yy < __height; yy++)
//        {
//            for(int xx = 0; xx < __width; xx++)
//            {
//                depthImage[CVD::ImageRef(xx,yy)] = (u_int16_t)(depth_array[yy*__width+xx]*15000);

//                normal_array_x.push_back(normal_array[ind+0]);
//                normal_array_y.push_back(normal_array[ind+1]);
//                normal_array_z.push_back(normal_array[ind+2]);

//                labelImage[CVD::ImageRef(xx,yy)] = label_vector.at(yy*__width+xx);


////                std::cout<<"length of normal = " << length(make_float3(normal_array[ind+0],
////                                                                       normal_array[ind+1],
////                                                                       normal_array[ind+2]));

//                ind+=3;

//            }
//        }

//        /// Clear the normal array
//        normal_array.clear();

//        sprintf(depthfileName,"../data/traj_02/normal_x_%04d.txt",i);
//        normal_file_x.open(depthfileName);

//        for(int idx = 0 ; idx < normal_array_x.size(); idx++)
//            normal_file_x << normal_array_x[idx]<<" ";

//        normal_array_x.clear();

//        sprintf(depthfileName,"../data/traj_02/normal_y_%04d.txt",i);
//        normal_file_y.open(depthfileName);

//        for(int idx = 0 ; idx < normal_array_y.size(); idx++)
//            normal_file_y << normal_array_y[idx]<<" ";

//        normal_array_y.clear();


//        sprintf(depthfileName,"../data/traj_02/normal_z_%04d.txt",i);
//        normal_file_z.open(depthfileName);

//        for(int idx = 0 ; idx < normal_array_z.size(); idx++)
//            normal_file_z << normal_array_z[idx]<<" ";

//        normal_array_z.clear();


//        normal_file_x << std::endl;
//        normal_file_y << std::endl;
//        normal_file_z << std::endl;

//        normal_file_x.close();
//        normal_file_y.close();
//        normal_file_z.close();

//    }

////    writeNormalImages(total_images);

//}




#include<iostream>
#include<VaFRIC/VaFRIC.h>
#include<TooN/Cholesky.h>
#include "./utils/se3utils.h"


int checkNormal(std::vector<float>& normal_array_x,
                std::vector<float>& normal_array_y,
                std::vector<float>& normal_array_z,
                int img_height,
                int img_width,
                std::string& normal_file_name_in_png)
{

    CVD::Image< CVD::Rgb<CVD::byte> >normalImage(CVD::ImageRef(img_width,img_height));

    int i = 0;

    for(int yy = 0; yy < img_height ; yy++ )
    {
        for(int xx = 0; xx < img_width; xx++ )
        {
            i = yy*img_width + xx;

            normalImage[CVD::ImageRef(xx,yy)] = CVD::Rgb<CVD::byte>(
                        (unsigned char)(normal_array_x[i]*127.f+127.f),
                        (unsigned char)(normal_array_y[i]*127.f+127.f),
                        (unsigned char)(normal_array_z[i]*127.f+127.f)
                        );
        }
    }

    CVD::img_save(normalImage,normal_file_name_in_png.c_str());

}


void writeNormalImages(int n_images,
                       std::string outputDirectory,
                       int width,
                       int height)
{
    for(int f = 0 ; f < n_images; f++ )
    {
        char normal_x_fileName[150];
        char normal_y_fileName[150];
        char normal_z_fileName[150];

        sprintf(normal_x_fileName,"%s/normal_x_%04d.txt",outputDirectory.c_str(),f);
        sprintf(normal_y_fileName,"%s/normal_y_%04d.txt",outputDirectory.c_str(),f);
        sprintf(normal_z_fileName,"%s/normal_z_%04d.txt",outputDirectory.c_str(),f);

        ifstream normal_file_x(normal_x_fileName);
        ifstream normal_file_y(normal_y_fileName);
        ifstream normal_file_z(normal_z_fileName);

        std::vector<float>normal_array_x;
        std::vector<float>normal_array_y;
        std::vector<float>normal_array_z;


        int i = 0;
        float val =0;
        while(1)
        {
            normal_file_x >> val;

            if ( normal_file_x.eof() )
            {
                break;
            }

            normal_array_x.push_back( val );

            i++;
        }

        i = 0;
        while(1)
        {
            normal_file_y >> val;

            if ( normal_file_y.eof() )
            {
                break;
            }

            normal_array_y.push_back( val );

            i++;
        }

        i = 0;
        while(1)
        {
            normal_file_z >> val;

            if ( normal_file_z.eof() )
            {
                break;
            }

            normal_array_z.push_back( val );

            i++;

        }

        char normalfileName[150];
        sprintf(normalfileName,"%s/normal_converted_image_%04d.png",outputDirectory.c_str(),f);

        std::string fileName(normalfileName);

        checkNormal(normal_array_x,
                    normal_array_y,
                    normal_array_z,
                    /*120*2,*/height,
                    /*160*2,*/width,
                    fileName);

        normal_array_x.clear();
        normal_array_y.clear();
        normal_array_z.clear();

        std::cout<<"converted : " << f << std::endl;
    }
}



int main(void)
{

    int __width  = 80*4;
    int __height = 60*4;

    float u0 = 319.5 / (640/__width);
    float v0 = 239.5 / (480/__height);

    float fx =   481.20 / (640/__width);
    float fy = - 480.00 / (480/__height);

    std::cout<<"u0 = " << u0 <<", v0 = " << v0 <<", fx = " << fx <<", fy = " << fy << std::endl;

//    std::string inputDirectory = "/scratch/workspace/code/new_scene_dataset/room_89/traj_01/160x120";

//    std::string inputDirectory = "/scratch/workspace/code/new_scene_dataset/room_89/new_chair";

    std::string inputDirectory = "/scratch/workspace/code/new_scene_dataset/room_89/BE457Chair";

//    std::string inputDirectory = "/scratch/workspace/code/new_scene_dataset/room_89/table_hemi";

//    std::string outputDirectory = "../data/hemi/table";

//    std::string outputDirectory = "../data/traj_01";

//    std::string outputDirectory = "../data/new_chair";

    std::string outputDirectory = "../data/BE457Chair";

    dataset::vaFRIC VaFRIC_dataset(inputDirectory.c_str(),
                                   __width,
                                   __height,
                                   u0,
                                   v0,
                                   fx,
                                   fy);

    //VaFRIC_dataset.writeLabelsToFile("label_colour_mapping.txt");

    int total_images = VaFRIC_dataset.getNumberofImageFiles();

    std::cout << "total_images = " << total_images << std::endl;

    float* depth_array = new float[__width*__height];

    std::vector<float>label_vector;

    std::vector<float>normal_array;

    CVD::Image<u_int16_t>depthImage(CVD::ImageRef(__width,__height));

    CVD::Image<CVD::byte>labelImage(CVD::ImageRef(__width,__height));

    CVD::Image<u_int16_t>heightImage(CVD::ImageRef(__width,__height));

    ofstream label_file;

    ofstream normal_file_x;
    ofstream normal_file_y;
    ofstream normal_file_z;
    ofstream height_from_ground;

    float4* points3D = new float4[__width*__height];


    TooN::Matrix<4>T =TooN::Data(-1, 0, 0, 0,
                                  0, 1, 0, 0,
                                  0, 0, 1, 0,
                                  0, 0, 0, 1);

    TooN::Cholesky<4>Tchangebasis(T);

    char depthfileName[250], labelfileName[250];

    VaFRIC_dataset.setNewClearPreviousColourLabels("label_colour_mapping.txt");

    ofstream posesFile("posesFile_BE457Chair_hemi.txt");


    for(int i = 0; i < total_images; i++)
    {

        std::cout<<"i = " << i << std::endl;

        VaFRIC_dataset.getEuclidean2PlanarDepth(i,0,depth_array);

        std::cout<<"read the euclidean depth" << std::endl;

        VaFRIC_dataset.getLabelsforImage(i,0,label_vector);

        std::cout<<"read the labels" << std::endl;

        VaFRIC_dataset.getNormalsFromDepth(i,0,normal_array);

        VaFRIC_dataset.get3Dpositions(i,0,points3D);

        sprintf(depthfileName,"%s/height_from_ground_%04d.txt",outputDirectory.c_str(),i);

        std::cout<<depthfileName<< std::endl;

        height_from_ground.open(depthfileName);

        TooN::SE3<>T_wc = VaFRIC_dataset.computeTpov_cam(i,0);

        se3_file_utils::change_basis(T_wc,Tchangebasis,T);

        posesFile << T_wc << std::endl;

        std::vector<float>normal_array_x;
        std::vector<float>normal_array_y;
        std::vector<float>normal_array_z;

        int ind = 0;

        for(int yy = 0; yy < __height; yy++)
        {
            for(int xx = 0; xx < __width; xx++)
            {
                /// depth is from [0-1]
                depthImage[CVD::ImageRef(xx,yy)] = (u_int16_t)(depth_array[yy*__width+xx]*5000);

                TooN::Vector<4>normal_ref = TooN::makeVector(normal_array[ind+0],
                                                             normal_array[ind+1],
                                                             normal_array[ind+2],
                                                             0.0f);

                TooN::Vector<4>normal_wref = T_wc * normal_ref;

                normal_array[ind+0] = normal_wref[0];
                normal_array[ind+1] = normal_wref[1];
                normal_array[ind+2] = normal_wref[2];


                normal_array_x.push_back(normal_array[ind+0]);
                normal_array_y.push_back(normal_array[ind+1] );
                normal_array_z.push_back(normal_array[ind+2]);

                labelImage[CVD::ImageRef(xx,yy)] = label_vector.at(yy*__width+xx);

                TooN::Vector<4> point_3D = TooN::makeVector(points3D[yy*__width+xx].x,
                                                            points3D[yy*__width+xx].y,
                                                            points3D[yy*__width+xx].z,
                                                            1.0f);

                TooN::Vector<4>point_w = T_wc * point_3D;

                /// Remember this needs to be divided by 10
                height_from_ground << point_w[1]/10 <<" ";

                ind+=3;

                heightImage[CVD::ImageRef(xx,yy)] = (u_int16_t)(15000 *(point_w[1]/10));

            }
        }

        char normal_file_name[150];
        sprintf(normal_file_name,"%s/normal_file_name_%04d.png",outputDirectory.c_str(),i);
        std::string normal_png_file(normal_file_name);
        VaFRIC_dataset.convertNormals2Image(i,0,normal_array,normal_png_file);


        sprintf(depthfileName,"%s/normal_x_%04d.txt",outputDirectory.c_str(),i);
        normal_file_x.open(depthfileName);

        for(int idx = 0 ; idx < normal_array_x.size(); idx++)
            normal_file_x << normal_array_x[idx]<<" ";

        normal_array_x.clear();

        sprintf(depthfileName,"%s/normal_y_%04d.txt",outputDirectory.c_str(),i);
        normal_file_y.open(depthfileName);

        for(int idx = 0 ; idx < normal_array_y.size(); idx++)
            normal_file_y << normal_array_y[idx]<<" ";

        normal_array_y.clear();


        sprintf(depthfileName,"%s/normal_z_%04d.txt",outputDirectory.c_str(),i);
        normal_file_z.open(depthfileName);

        for(int idx = 0 ; idx < normal_array_z.size(); idx++)
            normal_file_z << normal_array_z[idx]<<" ";

        normal_array_z.clear();


        normal_file_x << std::endl;
        normal_file_y << std::endl;
        normal_file_z << std::endl;
        height_from_ground << std::endl;

        normal_file_x.close();
        normal_file_y.close();
        normal_file_z.close();
        height_from_ground.close();

        /// Clear the label vector
        label_vector.clear();

        /// Clear the normal array
        normal_array.clear();

        sprintf(labelfileName,"%s/label_image_%04d.png",outputDirectory.c_str(),i);
        CVD::img_save(labelImage,labelfileName);

        sprintf(depthfileName,"%s/depth_image_%04d.png",outputDirectory.c_str(),i);
        CVD::img_save(depthImage,depthfileName);

        sprintf(depthfileName,"%s/height_image_%04d.png",outputDirectory.c_str(),i);
        CVD::img_save(heightImage,depthfileName);

        std::cout<<labelfileName<<std::endl;

    }

    posesFile.close();

    writeNormalImages(total_images,outputDirectory,__width,__height);

}

