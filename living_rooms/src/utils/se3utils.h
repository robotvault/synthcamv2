#pragma once

#include <iostream>
#include <string>
#include <vector>
#include <TooN/se3.h>
#include <TooN/Cholesky.h>

namespace se3_file_utils{

void change_basis(TooN::SE3<>& T_wc_ref,
                  TooN::Cholesky<4>& Tchangebasis,
                  TooN::Matrix<4>&T)
{
    TooN::Matrix<4>T4x4 = Tchangebasis.get_inverse()  * T_wc_ref * T ;

    TooN::Matrix<3>R_slice = TooN::Data(T4x4(0,0),T4x4(0,1),T4x4(0,2),
                                        T4x4(1,0),T4x4(1,1),T4x4(1,2),
                                        T4x4(2,0),T4x4(2,1),T4x4(2,2));


    TooN::Vector<3>t_slice = TooN::makeVector(T4x4(0,3),T4x4(1,3),T4x4(2,3));

    T_wc_ref = TooN::SE3<>(TooN::SO3<>(R_slice),t_slice);

}


inline void readSE3nativefile(std::vector<TooN::SE3<> >& poses,
                              std::string& fileName)

{
    std::ifstream ifile(fileName.c_str());

    if ( !ifile.is_open() )
    {
        std::cerr << "File cannot be opened. Exiting.." << std::endl;
        ifile.close();
        exit(1);
    }

    char readlinedata[300];
    float val=0.0f;

    while(1)
    {
        ifile.getline(readlinedata,300);
        if ( ifile.eof() )
            break;

//        std::cout << readlinedata << std::endl;

        std::istringstream istring(readlinedata);

        TooN::Vector<6>se3_vector = TooN::Zeros(6);

        istring >> val;
        se3_vector[0]=val;

        istring >> val;
        se3_vector[1]=val;

        istring >> val;
        se3_vector[2]=val;

        istring >> val;
        se3_vector[3]=val;

        istring >> val;
        se3_vector[4]=val;

        istring >> val;
        se3_vector[5]=val;

        TooN::SE3<> tT_wc = TooN::SE3<>(se3_vector);

//        std::cout << "tT_wc pose = " << tT_wc << std::endl;

        poses.push_back(tT_wc);
    }

    ifile.close();
}

}
