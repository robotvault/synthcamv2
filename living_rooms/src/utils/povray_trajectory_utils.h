#ifndef POVRAY_TRAJECTORY_UTILS_H_
#define POVRAY_TRAJECTORY_UTILS_H_
#include <TooN/TooN.h>
#include <string>
#include <fstream>
#include <sstream>
#include <TooN/se3.h>
#include <TooN/so3.h>
#include <vector>
#include <vector_types.h>
#include <omp.h>

#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <Eigen/Eigen>

using namespace std;
using namespace TooN;

inline void parse_string_get_pose ( char readlinedata[1200],  TooN::SE3<>& pose)
{
    std::istringstream iss(readlinedata);

    std::string token;
    size_t found;

    TooN::Matrix<3>RMatTranspose;
    TooN::Vector<3>TVec;

    if ( strstr(readlinedata, "scene_") != NULL)
    {
        while( iss >> token)
        {

            found = token.find("Declare");

            if ( found != string::npos )
            {
                istringstream str(token.substr(14,token.length()-13));

//                std::cout << "str = " << str.str() << std::endl;
                double val;

                str >> val;
//                std::cout << val << std::endl;

                if      ( token.find("val00") != string::npos)
                {
                    RMatTranspose(0,0)=val;
                }
                else if ( token.find("val01") != string::npos)
                {
                    RMatTranspose(1,0)=val;
                }
                else if ( token.find("val02") != string::npos )
                {
                    RMatTranspose(2,0)=val;
                }
                else if ( token.find("val10") != string::npos )
                {
                    RMatTranspose(0,1)=val;
                }
                else if ( token.find("val11") != string::npos )
                {
                    RMatTranspose(1,1)=val;
                }
                else if ( token.find("val12") != string::npos )
                {
                    RMatTranspose(2,1)=val;
                }
                else if ( token.find("val20") != string::npos )
                {
                    RMatTranspose(0,2)=val;
                }
                else if ( token.find("val21") != string::npos )
                {
                    RMatTranspose(1,2)=val;
                }
                else if ( token.find("val22") != string::npos )
                {
                    RMatTranspose(2,2)=val;
                }
                else if ( token.find("val30") != string::npos )
                {
                    TVec[0]=val;
                }
                else if ( token.find("val31") != string::npos )
                {
                    TVec[1]=val;
                }
                else if ( token.find("val32") != string::npos )
                {
                    TVec[2]=val;
                }
            }
        }


        TooN::SO3<>R(RMatTranspose);
        pose = TooN::SE3<>(R,TVec);

    }
    else
    {
        pose = TooN::SE3<>();
    }

//    std::cout << pose << std::endl;

}


inline void ModifyPosesThisTrajectory(std::string& fileName, std::string& outfileName)
{
    ifstream ifile(fileName.c_str());

    ofstream ofile(outfileName.c_str());
    char readlinedata[1200];

    TooN::SE3<> cur_pose;

    int posenum = 0;

    char outputfilename[50];
    char radiosityfilename[50];


    if ( ifile.is_open() )
    {
        while(1)
        {
            ifile.getline(readlinedata,1200);

            if ( ifile.eof() ) break;

            parse_string_get_pose(readlinedata,cur_pose);

            if ( cur_pose.get_translation() == makeVector(0,0,0))
                continue;

            {

                std::cout << readlinedata << std::endl;
                std::cout << cur_pose << std::endl;


                TooN::SE3<>modified_pose (cur_pose.get_rotation(),cur_pose.get_translation() + makeVector(-60,0,100));


                TooN::Matrix<> RMat = modified_pose.get_rotation().get_matrix();
                TooN::Vector<> TMat = modified_pose.get_translation();

                TooN::Matrix<>RMatTranspose = RMat.T();

                sprintf(outputfilename,"scene_%03d.png",posenum);
                sprintf(radiosityfilename,"scene_radiosity_%03d.txt",posenum);

                ofile << "/homes/ahanda/povray.3.7.0.rc3.withdepthmap/unix/povray +Ioffice.pov +O"<<outputfilename<<" +W320 +H240 +wt64 ";
                ofile << "+ Declare=val00="<<RMatTranspose(0,0)<< " + Declare=val01="<<RMatTranspose(0,1) << " + Declare=val02="<< RMatTranspose(0,2);
                ofile << "+ Declare=val10="<<RMatTranspose(1,0)<< " + Declare=val11="<<RMatTranspose(1,1) << " + Declare=val12="<< RMatTranspose(1,2);
                ofile << "+ Declare=val20="<<RMatTranspose(2,0)<< " + Declare=val21="<<RMatTranspose(2,1) << " + Declare=val22="<< RMatTranspose(2,2);
                ofile << "+ Declare=val30="<<TMat[0]         <<"  + Declare=val31="<<TMat[1]          << " + Declare=val32="<< TMat[2];
                ofile << " +HIfirst_pass_settings.inc" ;
                ofile << " +RFradiosity_first_pass/"<<radiosityfilename << " +RFO -d +L/homes/ahanda/povray.3.7.0.rc3.withdepthmap/include +LLightsysIV +Lmaps" << endl;


                std::cout << modified_pose << std::endl;

                posenum++;
            }

        }
    }

    ofile.close();



}


inline void ObtainPosesParseInputFile(char* inputfileName, std::vector < TooN::SE3<> >& poses_vector)
{

    std::ifstream ifile(inputfileName);

    char readlinedata[1200];

    TooN::SE3<> cam_pose;

    while(1)
    {
        ifile.getline(readlinedata,1200);

        if (ifile.eof())
            break;

        parse_string_get_pose(readlinedata,cam_pose);

        poses_vector.push_back ( cam_pose );
    }

    ifile.close();
}


inline TooN::SE3<> computeTpov_cam(int ref_img_no, int which_blur_sample)
{
    char text_file_name[360];

//    sprintf(text_file_name,"/home/ankur/workspace/GT/SlowTraj/80fps/scene_%02d_%04d.txt",which_blur_sample,(int)ref_img_no);

    sprintf(text_file_name,"/home/ankur/workspace/GT/Traj1/20fps/ref_images/scene_%02d_%04d.txt",which_blur_sample,(int)ref_img_no);

    cout << "text_file_name = " << text_file_name << endl;

    ifstream cam_pars_file(text_file_name);

    char readlinedata[300];

    float4 direction;
    float4 upvector;
    TooN::Vector<3>posvector;


    while(1)
    {
        cam_pars_file.getline(readlinedata,300);

        if ( cam_pars_file.eof())
            break;


        istringstream iss;


        if ( strstr(readlinedata,"cam_dir")!= NULL)
        {


            string cam_dir_str(readlinedata);

            cam_dir_str = cam_dir_str.substr(cam_dir_str.find("= [")+3);
            cam_dir_str = cam_dir_str.substr(0,cam_dir_str.find("]"));

            iss.str(cam_dir_str);
            iss >> direction.x ;
            iss.ignore(1,',');
            iss >> direction.y ;
            iss.ignore(1,',') ;
            iss >> direction.z;
            iss.ignore(1,',');
            cout << direction.x<< ", "<< direction.y << ", "<< direction.z << endl;
            direction.w = 0.0f;

        }

        if ( strstr(readlinedata,"cam_up")!= NULL)
        {

            string cam_up_str(readlinedata);

            cam_up_str = cam_up_str.substr(cam_up_str.find("= [")+3);
            cam_up_str = cam_up_str.substr(0,cam_up_str.find("]"));


            iss.str(cam_up_str);
            iss >> upvector.x ;
            iss.ignore(1,',');
            iss >> upvector.y ;
            iss.ignore(1,',');
            iss >> upvector.z ;
            iss.ignore(1,',');


            upvector.w = 0.0f;

        }

        if ( strstr(readlinedata,"cam_pos")!= NULL)
        {
            string cam_pos_str(readlinedata);

            cam_pos_str = cam_pos_str.substr(cam_pos_str.find("= [")+3);
            cam_pos_str = cam_pos_str.substr(0,cam_pos_str.find("]"));

            iss.str(cam_pos_str);
            iss >> posvector[0] ;
            iss.ignore(1,',');
            iss >> posvector[1] ;
            iss.ignore(1,',');
            iss >> posvector[2] ;
            iss.ignore(1,',');

        }

    }

    /// z = dir / norm(dir)
    Vector<3> z;
    z[0] = direction.x;
    z[1] = direction.y;
    z[2] = direction.z;
    normalize(z);

    /// x = cross(cam_up, z)
    Vector<3> x = Zeros(3);
    x[0] =  upvector.y * z[2] - upvector.z * z[1];
    x[1] =  upvector.z * z[0] - upvector.x * z[2];
    x[2] =  upvector.x * z[1] - upvector.y * z[0];

    normalize(x);

    /// y = cross(z,x)
    Vector<3> y = Zeros(3);
    y[0] =  z[1] * x[2] - z[2] * x[1];
    y[1] =  z[2] * x[0] - z[0] * x[2];
    y[2] =  z[0] * x[1] - z[1] * x[0];

    Matrix<3,3> R = Zeros(3,3);
    R[0][0] = x[0];
    R[1][0] = x[1];
    R[2][0] = x[2];

    R[0][1] = y[0];
    R[1][1] = y[1];
    R[2][1] = y[2];

    R[0][2] = z[0];
    R[1][2] = z[1];
    R[2][2] = z[2];

    cout << "SE3 pose = " << TooN::SE3<>(R, posvector) << endl;

    return TooN::SE3<>(R, posvector);

}


inline void Fill3Dpoints(float4* h_points3D, int ref_img_no, float K[3][3], bool add_depth)
{

    int width   = (int)(K[0][2]*2 +1);
    int height  = (int)(K[1][2]*2 +1);

    float u0 = K[0][2];
    float v0 = K[1][2];

    float fx = K[0][0];
    float fy = K[1][1]; /// Make sure it is negative

    assert(fy<0);

    ifstream ifile;

    char depth_file_name[60];

    sprintf(depth_file_name,"../data/scene_00_%04d.depth",ref_img_no);
//    sprintf(depth_file_name,"Lambertian%02d.depth",ref_img_no);

    cout << "depth_file_name =" << depth_file_name << endl;

    ifile.open(depth_file_name);

    // If depth file is stored another way
    double depth_array[width*height];
    for(int i = 0 ; i < height ; i++)
    {
        for (int j = 0 ; j < width ; j++)
        {
            double val = 0;
            ifile >> val;//depth_array[i*width+j];
            depth_array[i*width+j] = val;
        }
    }
    ifile.close();

    /// Convert into 3D points.

    for(int v = 0 ; v < height ; v++)
    {
        for(int u = 0 ; u < width ; u++)
        {
            float z =  depth_array[u+v*width] / sqrt(((u-u0)/fx)*((u-u0)/fx) + ((v-v0)/fy)*((v-v0)/fy) + 1 ) ;

            if ( add_depth )
                h_points3D[u+v*width].z = h_points3D[u+v*width].z + z;
            else
                h_points3D[u+v*width].z = z;

            h_points3D[u+v*width].y = ((v-v0)/fy)*(z);
            h_points3D[u+v*width].x = ((u-u0)/fx)*(z);
            h_points3D[u+v*width].w = 1.0f;
        }
    }

}


inline void  Fill3Dpoints_wc(float4* h_points3D,int ref_img_no, float K[3][3], TooN::SE3<>& cam_pose_wc)
{

    int width = (int)(K[0][2]*2 +1);
    int height  = (int)(K[1][2]*2 +1);

    float u0 = K[0][2];
    float v0 = K[1][2];

    float fx = K[0][0];
    float fy = K[1][1]; /// Make sure it is negative

    assert(fy<0);

    ifstream ifile;

    char depth_file_name[260];

    sprintf(depth_file_name,"../data/scene_000.depth");

    cout << "depth_file_name =" << depth_file_name << endl;

    ifile.open(depth_file_name);

    if ( ifile.is_open() )
    {
        std::cout <<" File can be opened" << std::endl;
//        getchar();
    }

    // If depth file is stored another way
    double depth_array[width*height];
    for(int i = 0 ; i < height ; i++)
    {
        for (int j = 0 ; j < width ; j++)
        {
            double val = 0;

            ifile >> val;//depth_array[i*width+j];

//            std::cout << "depth = " << val << std::endl;
//            getchar();

            depth_array[i*width+j] = val;

        }
    }
    ifile.close();

    /// Convert into 3D points.
    for(int v = 0 ; v < height ; v++)
    {
        for(int u = 0 ; u < width ; u++)
        {
            float z =  depth_array[u+v*width] / sqrt(((u-u0)/fx)*((u-u0)/fx) + ((v-v0)/fy)*((v-v0)/fy) + 1 ) ;

            Vector<3> cam_point = makeVector(((u-u0)/fx)*(z),((v-v0)/fy)*(z),z);

            TooN::Matrix<>R = cam_pose_wc.get_rotation().get_matrix();
            TooN::Vector<>t = cam_pose_wc.get_translation();

            Vector<3> new_point = R*cam_point + t;

            h_points3D[u+v*width].z = new_point[2];
            h_points3D[u+v*width].y = new_point[1];
            h_points3D[u+v*width].x = new_point[0];
            h_points3D[u+v*width].w = 1.0f;


//            std::cout <<"h_points3D = " << new_point[0] << ", " << new_point[1] <<", "<<new_point[2]<< std::endl;
        }
    }

}


inline void ReadPOVRenderedPoses(std::vector < TooN::SE3<> >& cam_poses_newtraj)
{
    int fps=80;
    if (cam_poses_newtraj.size())
        cam_poses_newtraj.clear();
    for(int pose_num = 0 ; pose_num <fps*5; pose_num++ )
    {
        cam_poses_newtraj.push_back(computeTpov_cam(pose_num,0));
    }
}

inline void generate_POVRay_commands(string& filename)
{

    std::ifstream ifile(filename.c_str());

    std::string outputfName = filename.substr(0,filename.length()-4) + "RenderingCommands.sh";

    std::ofstream ofile(outputfName.c_str());


    char readlinedata[200];

    /// Storage variales for file names
    char outputfilename[40];
    char radiosityfilename[60];


    /// Read the comments

    /// First comment
//    ifile.getline(readlinedata,200);
//    /// Second comment
//    ifile.getline(readlinedata,200);
//    /// Third Comment  - can extract fps
//    ifile.getline(readlinedata,200);
//    /// Fourth Comment - can extract time
//    ifile.getline(readlinedata,200);
//    /// Fifth comment  - new line character
//    ifile.getline(readlinedata,200);

    /// Read on the poses
//    TooN::Matrix<> Tpov_cam = Zeros(3,4);
    TooN::SE3<> Tpov_cam;
    int posenum = 0;

    while(1)
    {
        ifile >> Tpov_cam ;
        ifile.getline(readlinedata,200);


        if( ifile.eof())
            break;

        TooN::Matrix<> RMat = Tpov_cam.get_rotation().get_matrix();
        TooN::Vector<> TMat = Tpov_cam.get_translation();

        cout <<"Tpov_cam = " << Tpov_cam << endl;
        cout <<" TMat = " << TMat[0] << ", "<< TMat[1] << ", "<< TMat[2] << endl;


        TooN::Matrix<>RMatTranspose = RMat.T();

        sprintf(outputfilename,"scene_%03d.png",posenum);
        sprintf(radiosityfilename,"scene_radiosity_%03d.txt",posenum);

        ofile << "/scratch/workspace/povray.3.7.0.rc3.withdepthmap/unix/povray +Iphantom_POV_scene.pov +O"<<outputfilename<<" +W640 +H480 +wt12 ";
        ofile << "+ Declare=val00="<<RMatTranspose(0,0)<< " + Declare=val01="<<RMatTranspose(0,1) << " + Declare=val02="<< RMatTranspose(0,2);
        ofile << "+ Declare=val10="<<RMatTranspose(1,0)<< " + Declare=val11="<<RMatTranspose(1,1) << " + Declare=val12="<< RMatTranspose(1,2);
        ofile << "+ Declare=val20="<<RMatTranspose(2,0)<< " + Declare=val21="<<RMatTranspose(2,1) << " + Declare=val22="<< RMatTranspose(2,2);
        ofile << "+ Declare=val30="<<TMat[0]         <<"  + Declare=val31="<<TMat[1]          << " + Declare=val32="<< TMat[2];
        ofile << " -d" << endl;


//        ofile << "/homes/ahanda/povray.3.7.0.rc3.withdepthmap/unix/povray +Ioffice.pov +O"<<outputfilename<<" +W320 +H240 +wt64 ";
//        ofile << "+ Declare=val00="<<RMatTranspose(0,0)<< " + Declare=val01="<<RMatTranspose(0,1) << " + Declare=val02="<< RMatTranspose(0,2);
//        ofile << "+ Declare=val10="<<RMatTranspose(1,0)<< " + Declare=val11="<<RMatTranspose(1,1) << " + Declare=val12="<< RMatTranspose(1,2);
//        ofile << "+ Declare=val20="<<RMatTranspose(2,0)<< " + Declare=val21="<<RMatTranspose(2,1) << " + Declare=val22="<< RMatTranspose(2,2);
//        ofile << "+ Declare=val30="<<TMat[0]         <<"  + Declare=val31="<<TMat[1]          << " + Declare=val32="<< TMat[2];
//        ofile << " +RFradiosity_first_pass/"<<radiosityfilename << " +RFO -d +L/homes/ahanda/povray.3.7.0.rc3.withdepthmap/include +LLightsysIV +Lmaps +HIfirst_pass_settings.inc" << endl;


        posenum++;

    }
    ofile.close();


}


inline void convertTUM2POVRayposes(std::string& fileName, std::vector<TooN::SE3<> >& poses)
{

    char readlinedata[400];

    ifstream ifile(fileName.c_str());

    while(1)
    {

        ifile.getline(readlinedata,400);

        if ( ifile.eof())
            break;

        istringstream iss(readlinedata);

        float tx, ty, tz;
        float qx, qy, qz, qw;
        float tstamp;

        iss >> tstamp;

        iss >> tx; iss >> ty; iss >> tz;
        iss >> qx; iss >> qy; iss >> qz; iss >> qw;

        Eigen::Isometry3f T_wc;
        T_wc.setIdentity();

        Eigen::Quaternionf q(qw, qx, qy, qz);
        Eigen::Vector3f t(tx, ty, tz);

        T_wc.pretranslate(t).rotate(q);

        TooN::Matrix<3>rotMat = TooN::Data(T_wc(0,0),T_wc(0,1),T_wc(0,2),
                                           T_wc(1,0),T_wc(1,1),T_wc(1,2),
                                           T_wc(2,0),T_wc(2,1),T_wc(2,2));

        TooN::SE3<> tT_wc = TooN::SE3<>(TooN::SO3<>(rotMat),TooN::makeVector(tx*100,ty*100,tz*100));

        std::cout << tT_wc << std::endl;

        poses.push_back(tT_wc);
    }

}


inline void ReadAngularVelocityFile(std::vector< TooN::SE3<> >& camera_poses_vec)
{

    if (camera_poses_vec.size())
        camera_poses_vec.clear();

    TooN::SO3<> R;
    float ang_vel;
    ifstream ifile("../data/imu_aligned_200_20_58.subsampled.txt");

//    for(int i = 0; i<=200; i++)
//    camera_poses_vec.push_back(TooN::SE3<>());

    if ( ifile.is_open() )
    {
        while(1)
        {
            ifile >> ang_vel;

            if ( ifile.eof() ) break;

            TooN::SO3<> Rworld_cam = TooN::SO3<>(makeVector(0,0,ang_vel/200));
            R = R*Rworld_cam;

            TooN::SE3<> Tworld_cam = TooN::SE3<>(R,makeVector(0,0,0));

            camera_poses_vec.push_back(Tworld_cam);
        }

    }

    ifile.close();

}

#endif
