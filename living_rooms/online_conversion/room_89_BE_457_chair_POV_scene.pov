//==================================================
//POV-Ray Main scene file
//==================================================
//This file has been created by PoseRay v3.13.23.587
//3D model to POV-Ray/Moray Converter.
//Author: FlyerX
//Email: flyerx_2000@yahoo.com
//Web: https://sites.google.com/site/poseray/
//==================================================
//Files needed to run the POV-Ray scene:
//room_89_BE_457_chair_POV_main.ini (initialization file - open this to render)
//room_89_BE_457_chair_POV_scene.pov (scene setup of cameras, lights and geometry)
//room_89_BE_457_chair_POV_geom.inc (geometry)
//room_89_BE_457_chair_POV_mat.inc (materials)
//1006823-.JPG
//1006821-.JPG
// 
//==================================================
//Model Statistics:
//Number of triangular faces..... 34588
//Number of vertices............. 21778
//Number of normals.............. 24556
//Number of UV coordinates....... 13391
//Number of lines................ 14
//Number of materials............ 15
//Number of groups/meshes........ 2
//Number of subdivision faces.... 0
//UV boundaries........ from u,v=(-21967.19,-6.8594)
//                        to u,v=(21966.85,15909.15)
//Bounding Box....... from x,y,z=(-4.801283,-0.106352,-2.437092)
//                      to x,y,z=(2.522122,2.794712,3.310522)
//                 size dx,dy,dz=(7.323405,2.901064,5.747614)
//                  center x,y,z=(-1.13958,1.34418,0.436715)
//                       diagonal 9.751077
//Surface area................... 165.7884
//             Smallest face area 1.001748E-10
//              Largest face area 21.04605
//Memory allocated for geometry.. 3 MBytes
// 
//==================================================
//IMPORTANT:
//This file was designed to run with the following command line options: 
// +W320 +H240 +FN +AM1 +A0.3 +r3 +Q9 +C -UA +MV3.7
//if you are not using an INI file copy and paste the text above to the command line box before rendering
 
 
global_settings {
  //This setting is for alpha transparency to work properly.
  //Increase by a small amount if transparent areas appear dark.
   max_trace_level 15
   assumed_gamma 1
 
}
#include "room_89_BE_457_chair_POV_geom.inc" //Geometry
 
//CAMERA PoseRayCAMERA
camera {
        perspective
        up <0,1,0>
        right -x*image_width/image_height
        location <5E-7,-2.220446E-16,19.50216>
        look_at <5E-7,-2.220446E-16,18.50216>
        angle 32.93461 // horizontal FOV angle
        rotate <0,0,0> //roll
        rotate <-25,0,0> //pitch
        rotate <0,45,0> //yaw
        translate <-1.13958,1.34418,0.436715>
        }
 
//PoseRay default Light attached to the camera
light_source {
              <4.99999999847844E-7,-2.22044604925031E-16,19.502155> //light position
              color rgb <1,1,1>*1.6
              parallel
              point_at <5E-7,-2.220446E-16,0>
              rotate <0,0,0> //roll
              rotate <-25,0,0> //elevation
              rotate <0,45,0> //rotation
             }
 
//Background
background { color srgb<1,1,1>  }
 
//Assembled object that is contained in room_89_BE_457_chair_POV_geom.inc with no SSLT components
object{
      room_89_BE_457_chair_
      }
//==================================================
