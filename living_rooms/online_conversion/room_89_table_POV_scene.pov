//==================================================
//POV-Ray Main scene file
//==================================================
//This file has been created by PoseRay v3.13.23.587
//3D model to POV-Ray/Moray Converter.
//Author: FlyerX
//Email: flyerx_2000@yahoo.com
//Web: https://sites.google.com/site/poseray/
//==================================================
//Files needed to run the POV-Ray scene:
//room_89_table_POV_main.ini (initialization file - open this to render)
//room_89_table_POV_scene.pov (scene setup of cameras, lights and geometry)
//room_89_table_POV_geom.inc (geometry)
//room_89_table_POV_mat.inc (materials)
//1006821-.JPG
//1006823-.JPG
// 
//==================================================
//Model Statistics:
//Number of triangular faces..... 10402
//Number of vertices............. 5809
//Number of normals.............. 10287
//Number of UV coordinates....... 2584
//Number of lines................ 14
//Number of materials............ 21
//Number of groups/meshes........ 2
//Number of subdivision faces.... 0
//UV boundaries........ from u,v=(0,0)
//                        to u,v=(1,1)
//Bounding Box....... from x,y,z=(-4.801283,-0.138771,-2.437092)
//                      to x,y,z=(2.522122,2.794712,3.310522)
//                 size dx,dy,dz=(7.323405,2.933483,5.747614)
//                  center x,y,z=(-1.13958,1.32797,0.436715)
//                       diagonal 9.760771
//Surface area................... 206.423
//             Smallest face area 4.837705E-7
//              Largest face area 21.04605
//Memory allocated for geometry.. 1 MByte
// 
//==================================================
//IMPORTANT:
//This file was designed to run with the following command line options: 
// +W320 +H240 +FN +AM1 +A0.3 +r3 +Q9 +C -UA +MV3.7
//if you are not using an INI file copy and paste the text above to the command line box before rendering
 
 
global_settings {
  //This setting is for alpha transparency to work properly.
  //Increase by a small amount if transparent areas appear dark.
   max_trace_level 15
   assumed_gamma 1
 
}
#include "room_89_table_POV_geom.inc" //Geometry
 
//CAMERA PoseRayCAMERA
camera {
        perspective
        up <0,1,0>
        right -x*image_width/image_height
	transform {
	matrix <    val00,    val01,  val02,
	   val10,   val11,    val12,
	   val20,   val21,    val22,
	   val30,   val31, val32>
	}

//PoseRay default Light attached to the camera

//Background
background { color srgb<1,1,1>  }
 
//Assembled object that is contained in room_89_table_POV_geom.inc with no SSLT components
object{
      room_89_table_
      }
//==================================================
