//==================================================
//POV-Ray Main scene file
//==================================================
//This file has been created by PoseRay v3.13.23.587
//3D model to POV-Ray/Moray Converter.
//Author: FlyerX
//Email: flyerx_2000@yahoo.com
//Web: https://sites.google.com/site/poseray/
//==================================================
//Files needed to run the POV-Ray scene:
//room_89_chair_POV_main.ini (initialization file - open this to render)
//room_89_chair_POV_scene.pov (scene setup of cameras, lights and geometry)
//room_89_chair_POV_geom.inc (geometry)
//room_89_chair_POV_mat.inc (materials)
//1006821-.JPG
//1006823-.JPG
// 
//==================================================
//Model Statistics:
//Number of triangular faces..... 1038
//Number of vertices............. 625
//Number of normals.............. 1095
//Number of UV coordinates....... 333
//Number of lines................ 14
//Number of materials............ 14
//Number of groups/meshes........ 2
//Number of subdivision faces.... 0
//UV boundaries........ from u,v=(0,0)
//                        to u,v=(1,1)
//Bounding Box....... from x,y,z=(-4.801283,-0.106352,-2.437092)
//                      to x,y,z=(2.522122,2.794712,3.310522)
//                 size dx,dy,dz=(7.323405,2.901064,5.747614)
//                  center x,y,z=(-1.13958,1.34418,0.436715)
//                       diagonal 9.751077
//Surface area................... 164.1398
//             Smallest face area 2.565522E-6
//              Largest face area 21.04605
//Memory allocated for geometry.. 157 KBytes
// 
//==================================================
//IMPORTANT:
//This file was designed to run with the following command line options: 
// +W320 +H240 +FN +AM1 +A0.3 +r3 +Q9 +C -UA +MV3.7
//if you are not using an INI file copy and paste the text above to the command line box before rendering
 
 
global_settings {
  //This setting is for alpha transparency to work properly.
  //Increase by a small amount if transparent areas appear dark.
   max_trace_level 15
   assumed_gamma 1
 
}
#include "room_89_chair_POV_geom.inc" //Geometry
 
//CAMERA PoseRayCAMERA
//CAMERA PoseRayCAMERA
camera {

        perspective
        up <0,1,0>
        //right -x*image_width/image_height
	angle 150
	transform {
	matrix <    val00,    val01,  val02,
	   val10,   val11,    val12,
	   val20,   val21,    val22,
	   val30,   val31, val32>
	}

}





/*#macro Axis_( AxisLen, Dark_Texture,Light_Texture)
 union{
    cylinder { <0,-AxisLen*6,0>,<0,AxisLen,0>,0.05
               texture{checker texture{Dark_Texture }
                               texture{Light_Texture}
                       translate<0.1,0,0.1>}
             }
    cone{<0,AxisLen,0>,0.2,<0,AxisLen+0.7,0>,0
          texture{Dark_Texture}
         }
     } // end of union
#end // of macro "Axis()"
*/

#macro Axis_( AxisLen, Colour)
 union{
    cylinder { <0,-AxisLen*6,0>,<0,AxisLen,0>,0.05
               texture{pigment{color Colour}
                       translate<0.1,0,0.1>}
             }
    cone{<0,AxisLen,0>,0.2,<0,AxisLen+0.7,0>,0
          texture{pigment{color Colour}}
         }
     } // end of union
#end // of macro "Axis()"


//------------------------------------------------------------------------
#macro AxisXYZ( AxisLenX, AxisLenY, AxisLenZ, Tex_Dark, Tex_Light)
//--------------------- drawing of 3 Axes --------------------------------
union{
#if (AxisLenX != 0)
 object { Axis_(AxisLenX, Red)   rotate< 0,0,-90>}// x-Axis
 text   { ttf "Ubuntu-R.ttf",  "x",  0.15,  0  texture{ pigment{color Red} }
          scale 0.5 translate <AxisLenX+0.15,0.2,-0.05>}
#end // of #if
#if (AxisLenY != 0)
 object { Axis_(AxisLenY, Green)   rotate< 0,0,  0>}// y-Axis
 text   { ttf "Ubuntu-R.ttf",  "y",  0.15,  0  texture{Tex_Dark}
           scale 0.5 translate <-0.45,AxisLenY+0.20,-0.05>}
#end // of #if
#if (AxisLenZ != 0)
 object { Axis_(AxisLenZ, Blue)   rotate<90,0,  0>}// z-Axis
 text   { ttf "Ubuntu-R.ttf",  "z",  0.15,  0  texture{Tex_Dark}
               scale 0.5 translate <-0.75,0.2,AxisLenZ+0.10>}
#end // of #if
} // end of union
#end// of macro "AxisXYZ( ... )"
//-------------------------------------------------------------------------
#declare Texture_A_Dark  = texture {
                               pigment{ color rgb<1,0.55,0>}
                               finish { phong 1}
                             }
#declare Texture_A_Light = texture {
                               pigment{ color rgb<1,1,1>}
                               finish { phong 1}
                             }

//---------------------------------------------------------------------------
object{ AxisXYZ( 2.65, 6.45, 5, Texture_A_Dark, Texture_A_Light) scale 0.5 translate <-1.5, 0.5,0.3>} 
//Background
background { color srgb<1,1,1>  }
 
//Assembled object that is contained in room_89_chair_POV_geom.inc with no SSLT components
object{
      room_89_chair_
      }
//==================================================
