//==================================================
//POV-Ray Main scene file
//==================================================
//This file has been created by PoseRay v3.13.23.587
//3D model to POV-Ray/Moray Converter.
//Author: FlyerX
//Email: flyerx_2000@yahoo.com
//Web: https://sites.google.com/site/poseray/
//==================================================
//Files needed to run the POV-Ray scene:
//room_89_with_unknowns_POV_main.ini (initialization file - open this to render)
//room_89_with_unknowns_POV_scene.pov (scene setup of cameras, lights and geometry)
//room_89_with_unknowns_POV_geom.inc (geometry)
//room_89_with_unknowns_POV_mat.inc (materials)
//black_wood_l.jpg
//Body1.png
//woodtexturepatternipadwallpaperpng-x.jpg
//1006823-.JPG
//1006821-.JPG
// 
//==================================================
//Model Statistics:
//Number of triangular faces..... 1709778
//Number of vertices............. 4934504
//Number of normals.............. 4940833
//Number of UV coordinates....... 20952
//Number of lines................ 14
//Number of materials............ 31
//Number of groups/meshes........ 2
//Number of subdivision faces.... 0
//UV boundaries........ from u,v=(-1.90395,-3.51033)
//                        to u,v=(1.52006,1)
//Bounding Box....... from x,y,z=(-4.818482,-0.203836,-2.437092)
//                      to x,y,z=(2.522122,2.794712,3.310522)
//                 size dx,dy,dz=(7.340604,2.998548,5.747614)
//                  center x,y,z=(-1.14818,1.295438,0.436715)
//                       diagonal 9.793407
//Surface area................... 237.1176
//             Smallest face area 1.000775E-10
//              Largest face area 21.04605
//Memory allocated for geometry.. 380 MBytes
// 
//==================================================
//IMPORTANT:
//This file was designed to run with the following command line options: 
// +W320 +H240 +FN -A +Q9 +C -UA +MV3.7
//if you are not using an INI file copy and paste the text above to the command line box before rendering
 
#include "rad_def.inc"
 
global_settings {
  //This setting is for alpha transparency to work properly.
  //Increase by a small amount if transparent areas appear dark.
   max_trace_level 15
   assumed_gamma 1
 
   #local p_start	        =	64/max(image_width,image_height);
   #local p_end_final	=	4/max(image_width,image_height);
radiosity {
     pretrace_start p_start
     pretrace_end   p_end_final 
     count 100
     nearest_count 5
     error_bound 0.05
     recursion_limit 1
     low_error_factor .5
     gray_threshold 0.0
     minimum_reuse 0.015
     brightness 1
     adc_bailout 0.01/2
}
}
#include "room_89_with_unknowns_POV_geom.inc" //Geometry
 
//CAMERA PoseRayCAMERA
camera {
        perspective
        up <0,1,0>
        right -x*image_width/image_height
        location <2.220446E-16,0,19.58682>
        look_at <2.220446E-16,0,18.58682>
        angle 32.93461 // horizontal FOV angle
        rotate <0,0,28.8939> //roll
        rotate <-24.77612,0,0> //pitch
        rotate <0,49.13614,0> //yaw
        translate <-1.14818,1.295438,0.436715>
        }
 
//PoseRay default Light attached to the camera
light_source {
              <2.22044604925031E-16,0,19.586815> //light position
              color rgb <1,1,1>*1.6
              parallel
              point_at <2.220446E-16,0,0>
              rotate <0,0,28.8939> //roll
              rotate <-24.77612,0,0> //elevation
              rotate <0,49.13614,0> //rotation
             }
 
//Background
background { color srgb<1,1,1>  }
 
//Assembled object that is contained in room_89_with_unknowns_POV_geom.inc with no SSLT components
object{
      room_89_with_unknowns_
      }
//==================================================
