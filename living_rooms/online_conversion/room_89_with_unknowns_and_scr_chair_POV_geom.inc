//==================================================
//POV-Ray Geometry file
//==================================================
//This file has been created by PoseRay v3.13.23.587
//3D model to POV-Ray/Moray Converter.
//Author: FlyerX
//Email: flyerx_2000@yahoo.com
//Web: https://sites.google.com/site/poseray/
//==================================================
//Files needed to run the POV-Ray scene:
//room_89_with_unknowns_and_scr_chair_POV_main.ini (initialization file - open this to render)
//room_89_with_unknowns_and_scr_chair_POV_scene.pov (scene setup of cameras, lights and geometry)
//room_89_with_unknowns_and_scr_chair_POV_geom.inc (geometry)
//room_89_with_unknowns_and_scr_chair_POV_mat.inc (materials)
//black_wood_l.jpg
//Body1.png
//woodtexturepatternipadwallpaperpng-x.jpg
//1006823-.JPG
//1006821-.JPG
// 
//==================================================
//Model Statistics:
//Number of triangular faces..... 1712496
//Number of vertices............. 4936040
//Number of normals.............. 4943422
//Number of UV coordinates....... 21795
//Number of lines................ 14
//Number of materials............ 34
//Number of groups/meshes........ 2
//Number of subdivision faces.... 0
//UV boundaries........ from u,v=(-1.90395,-3.51033)
//                        to u,v=(1.52006,1)
//Bounding Box....... from x,y,z=(-4.818482,-0.203836,-2.437092)
//                      to x,y,z=(2.522122,2.794712,3.310522)
//                 size dx,dy,dz=(7.340604,2.998548,5.747614)
//                  center x,y,z=(-1.14818,1.295438,0.436715)
//                       diagonal 9.793407
//Surface area................... 241.4211
//             Smallest face area 1.000775E-10
//              Largest face area 21.04605
//Memory allocated for geometry.. 380 MBytes
// 
//==================================================
 
 
//Materials
#include "r